package main

import "fmt"

func main() {
	p := []int{2, 3, 4, 5, 6, 7, 8, 9, 10}
	fmt.Println(fmt.Sprintf("P %v", p))

	// Copy
	newP := make([]int, len(p))
	newP = append(p[:0:0], p...)
	fmt.Println(fmt.Sprintf("New P %v", newP))

}
