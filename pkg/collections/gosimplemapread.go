package main

import "fmt"

func main() {
	provinces := make(map[string]string)
	provinces["SP"] = "Sao Paulo"
	provinces["RJ"] = "Rio de Janeiro"
	fmt.Println("SP:", provinces["SP"])
}
