package main

import "fmt"

func main()  {
	golang := &Golang{"Hello Golang"}
	nodejs := &Nodejs{"Hello Nodejs"}

	show(golang)
	show(nodejs)
}

func show(l Language)  {
	fmt.Println(l.Log())
}

type Language interface {
	Log () string
}

type Golang struct {
	Value string
}

func (g *Golang) Log() string {
	return "fmt.Println(" + g.Value + ")"
}

type Nodejs struct {
	Value string
}

func (g *Nodejs) Log() string {
	return "console.log(" + g.Value + ")"
}