package main

import "fmt"

func main()  {
	people := People{"Jhon", 30}

	fmt.Println(people)
	fmt.Println(people.ToString())
}


type People struct {
	Name string `json:"name"`
	Age  int    `json:"age"`
}

func (p *People) ToString() string {
	return fmt.Sprintf("Name %s Age %d.", p.Name, p.Age)
}