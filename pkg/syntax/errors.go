package main

import (
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
)

var InvalidValue = errors.New("Invalid value")

func main() {

	err := setValue(5)
	if err != nil {
		fmt.Println("Message"+ err.Error())
	}


	request()
}

func setValue(value int) (err error) {
	if value < 10 {
		return InvalidValue
	}
	return nil
}


func request () {

	url := "http://www.mocky.io/v2/5d8b64413500006200d46f85"

	client := &http.Client{}

	request, err := http.NewRequest("GET", url, nil)
	if err != nil {
		fmt.Println(err.Error())
	}

	response, err := client.Do(request)
	if err != nil {
		fmt.Println(err.Error())
	}

	defer response.Body.Close()

	bodyBytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		fmt.Println(err.Error())
	}

	fmt.Println(string(bodyBytes))
}

