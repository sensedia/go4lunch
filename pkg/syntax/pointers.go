package main

import "fmt"

func zero(ival int) {
	ival = 0
}

func zeroPointers(iptr *int) {
	*iptr = 0
}

func main() {
	i := 1
	fmt.Println("Initial:", i)

	zero(i)
	fmt.Println("zero:", i)

	zeroPointers(&i)
	fmt.Println("zeroPointers:", i)

	fmt.Println("Pointer:", &i)
}