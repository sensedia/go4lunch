package main

import "fmt"

func main()  {
	fmt.Println(sum(10,20))
	fmt.Println(sumName(10,20))

	valorX, valorY := returns(5,10)
	fmt.Println(valorX, valorY)
}

func sum(a, b int) int  {
	return a + b
}

func sumName(a, b int) (sum int) {
	sum = a + b
	return sum
}

func returns(a, b int) (x, y  int) {
	x = a
	y = b
	return x, y
}