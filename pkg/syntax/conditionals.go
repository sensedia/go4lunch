package main

import "fmt"

func main() {
	name := "Jhon"

	if name == "Jhon" {
		fmt.Println("Hello Jhon")
	} else {
		fmt.Println("Hello" + name)
	}

	if value := 10; value < 0 {
		fmt.Println(value, "is negative")
	} else if value < 10 {
		fmt.Println(value, "has 1 digit")
	} else {
		fmt.Println(value, "has multiple digits")
	}
}