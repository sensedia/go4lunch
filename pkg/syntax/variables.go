package main

import "fmt"

var (
	x = 10
	y = 20
)

var Beer string

func main() {
	fmt.Println(Beer)
	fmt.Println(x, y)

	var lastName string
	var name = "Jhon"

	age := 18

	fmt.Println(name, lastName, age)

	var a, b, c = 10, 15, 20

	fmt.Println(a, b, c)
}
