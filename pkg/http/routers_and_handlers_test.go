package http

import (
	"log"
	"testing"
)

func TestRoutersAndHandlers(t *testing.T) {
	err := StartHTTPServer()
	if err != nil {
		log.Fatal(err)
		t.Error(err)
	}
}
