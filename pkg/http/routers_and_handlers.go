package http

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

var books = map[string]Book{
	"1": {"Galaxy's Edge: Black Spire (Star Wars)", "Delilah S. Dawson", 17.39},
	"2": {"The Bone Ships (The Tide Child Trilogy)", "RJ Barker", 12.63},
	"3": {"The Testaments: The Sequel to The Handmaid's Tale", "Margaret Atwood", 17.37}}

type Book struct {
	Title  string  `json:"title"`
	Author string  `json:"author"`
	Price  float64 `json:"price"`
}

func StartHTTPServer() error {
	mux := http.NewServeMux()
	mux.Handle("/books", getBookByKey())
	return http.ListenAndServe(":8080", mux)
}

func getBookByKey() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		log.Println("Received request")
		key := r.URL.Query().Get("key")
		if key == "" {
			http.Error(w, "Missing key name in query string", http.StatusBadRequest)
			log.Println("Answering 400")
			return
		}
		if book, contains := books[key]; contains {
			body, err := json.Marshal(book)
			if err != nil {
				http.Error(w, fmt.Sprintf("Error when marshelling book: %s", err), http.StatusInternalServerError)
				log.Println("Answering 500")
				return
			}
			w.WriteHeader(http.StatusOK)
			_, err = w.Write(body)
			if err != nil {
				http.Error(w, fmt.Sprintf("Error when wrinting book json on response body: %s", err), http.StatusInternalServerError)
				log.Println("Answering 500")
				return
			}
			log.Println("Answering 200")
			return
		}
		log.Println("Answering 404")
		http.Error(w, "Not found", http.StatusNotFound)
	}
}
